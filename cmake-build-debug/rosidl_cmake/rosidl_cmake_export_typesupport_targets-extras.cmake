# generated from
# rosidl_cmake/cmake/template/rosidl_cmake_export_typesupport_targets.cmake.in

set(_exported_typesupport_targets
  "__rosidl_typesupport_introspection_c:knapp_task_final__rosidl_typesupport_introspection_c;__rosidl_typesupport_introspection_cpp:knapp_task_final__rosidl_typesupport_introspection_cpp")

# populate knapp_task_final_TARGETS_<suffix>
if(NOT _exported_typesupport_targets STREQUAL "")
  # loop over typesupport targets
  foreach(_tuple ${_exported_typesupport_targets})
    string(REPLACE ":" ";" _tuple "${_tuple}")
    list(GET _tuple 0 _suffix)
    list(GET _tuple 1 _target)

    set(_target "knapp_task_final::${_target}")
    if(NOT TARGET "${_target}")
      # the exported target must exist
      message(WARNING "Package 'knapp_task_final' exports the typesupport target '${_target}' which doesn't exist")
    else()
      list(APPEND knapp_task_final_TARGETS${_suffix} "${_target}")
    endif()
  endforeach()
endif()
