//
// Created by Christian Horst on 23.07.21.
//

#ifndef KNAPP_TASK_FINAL_PATHPLANNER_H
#define KNAPP_TASK_FINAL_PATHPLANNER_H
#include "DataHandler.h"
namespace fs = std::filesystem;
using namespace std;

class PathPlanner {
public:
    PathPlanner();
    void setNumNodes(int nodes) {nodeNumber = nodes;};
    void initializeGraph(double robotX, double robotY, const std::list<DataHandler::Part>& reducedPartList);
    float calculateShortestPath(std::list<DataHandler::Part> reducedPartList);
    std::list<DataHandler::Part> sortPartListString(std::list<DataHandler::Part> partList);
    std::list<DataHandler::Part>  sortPartListNumber(std::list<DataHandler::Part> partList);
    std::list<DataHandler::Part> reducePartList(std::list<DataHandler::Part> partList);
    std::list<DataHandler::Part> rearrangePartOrder(std::list<DataHandler::Part> partList);

private:
    int nodeNumber;
    float **graph;
    vector<pair<int, string>>  currentPermutation;
    static bool sortByName(const DataHandler::Part &lhs, const DataHandler::Part &rhs){return lhs.partName < rhs.partName;};
    static bool sortByNumber(const DataHandler::Part &lhs, const DataHandler::Part &rhs){return lhs.pickupOrder < rhs.pickupOrder;};

};


#endif //KNAPP_TASK_FINAL_PATHPLANNER_H
