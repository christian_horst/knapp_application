//
// Created by Christian Horst on 23.07.21.
//

#include "PathPlanner.h"

PathPlanner::PathPlanner() {

}

std::list<DataHandler::Part> PathPlanner::sortPartListString(std::list<DataHandler::Part> partList) {
    partList.sort(sortByName);
    return partList;
}

std::list<DataHandler::Part> PathPlanner::sortPartListNumber(std::list<DataHandler::Part> partList) {
    partList.sort(sortByNumber);
    return partList;
}

std::list<DataHandler::Part> PathPlanner::reducePartList(std::list<DataHandler::Part> partList) {
    std::list<DataHandler::Part> reducedPartList;
    reducedPartList.push_back(partList.front());
    auto listEnd = partList.end().operator--();

    //Reduce the part list by checking for different strings for "Part A" , "Part B", etc.
    for(auto it = partList.begin(); it != partList.end(); ++it){
        if(it != listEnd){
            std::list<DataHandler::Part>::iterator secit;
            secit = std::next(it,1);
            if(it->partName != secit->partName) {
                reducedPartList.push_back(*secit);
            }
        }
    }
    return reducedPartList;
}

//this function returns the cost of the path with the lowest cost in order to visit all nodes
// the nodes represent the pickup locations for the parts
//the approach of this algorithm is a brute force approach of the travelling salesman problem
// the cost of each possible path from the pickup location to all nodes is calculated
//the path with lowest cost is the desired path
float PathPlanner::calculateShortestPath(std::list<DataHandler::Part> reducedPartList) {
    // Picking a source node which is the robot position
    int source = 0;
    //the use of a pair is important in order to keep track of the which part ("Part A" ...) belongs to which node
    vector<pair<int, string>> nodes;

    int startNodeNumber = 1;
    for(auto & it : reducedPartList){
        nodes.emplace_back(startNodeNumber,it.partName);
        ++startNodeNumber;
    }

    int n = nodes.size();
    float shortest_path = 5000000;
    /// generating permutations and tracking the minimum cost

    //For each possible path, the cost is calculated
    do{

        float path_weight = 0;

        int j = source;
        for (int i = 0; i < n; i++)
        {
            path_weight += graph[j][nodes[i].first];
            j = nodes[i].first;
        }
        // save the current order of the nodes to keep track of the shortest path
        if(shortest_path < path_weight){
            currentPermutation = nodes;
            shortest_path=shortest_path;
        }
        else{
            shortest_path = path_weight;
        }
        //shortest_path = min(shortest_path, path_weight);

    } while(next_permutation(nodes.begin(),nodes.end()));

    return shortest_path;
}

//This function initializes a matrix where the cost from each point to all the others is stored as a distance
// the diagonal values should be zero, since these values indicates the distance for each point to itself

void PathPlanner::initializeGraph(double robotX, double robotY, const std::list<DataHandler::Part>& reducedPartList) {
   std::vector<std::pair<double, double>> nodePositions;
   //store the positions of the nodes
   //the robot position is always node 0 since it is the starting node
   // The rest is ordered as: Node 1 Part A, Node 2 Part B and so on
   nodePositions.emplace_back(robotX,robotY);
    for(auto & it : reducedPartList){
        nodePositions.emplace_back(it.partX,it.partY);
    }

    int numberOfNodes= nodePositions.size();
    graph = new float *[numberOfNodes];
    for(int i = 0; i < numberOfNodes; i++) {
        graph[i] = new float[numberOfNodes];
        for(int j=0;j < numberOfNodes;j++)
        {
            //calculate the distances to each point
            graph[i][j] = sqrt(pow(nodePositions[i].first - nodePositions[j].first , 2) + pow(nodePositions[i].second - nodePositions[j].second, 2));

        }
    }
}
//This function orders all the parts in the optimal order
// This is necessary to print out the correct processing of the current order in the right order
std::list<DataHandler::Part> PathPlanner::rearrangePartOrder(std::list<DataHandler::Part> partList) {
    int nodeToVisit= 1;
    //assign pick up order to each part
    for(auto & it : currentPermutation ){
        for(auto & jt : partList){
            if(jt.partName == it.second) jt.pickupOrder = nodeToVisit;
        }
        ++nodeToVisit;
    }

    //sort the part list by its pickup order (1,2,3...)
    partList.sort(sortByNumber);
    return partList;
}

