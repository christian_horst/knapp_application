//
// CCreated by Christian Horst on 23.07.21.
//

#ifndef KNAPP_TASK_FINAL_FILEHANDLER_H
#define KNAPP_TASK_FINAL_FILEHANDLER_H
#include "DataHandler.h"
namespace fs = std::filesystem;
using namespace std;

class FileHandler {
public:
    FileHandler();
    int checkForFiles(const std::string& pathName);
    void retrieveOrderInformation(const string& pathName, int wantedID);
    void retrieveProductInformation();
    void setOrderPath(const std::string& pathName) {orderPathName=pathName;};
    void setConfigPath(const std::string& pathName) {configPathName=pathName;};
    void setOrderFound(bool order) { orderFound=order;};
    void clearData();
    bool checkOrderFound() const {return orderFound;};


    DataHandler::Order getOrderInfo() const {return currentOrder;};
    std::list<DataHandler::Part> getProductInfo() const {return partList;};


    set<fs::path> getFiles(string pathName);
private:

    int test =0;
    string orderPathName;
    string configPathName;
    DataHandler::Order currentOrder;
    std::list<DataHandler::Part> partList;
    bool orderFound;
};


#endif //KNAPP_TASK_FINAL_FILEHANDLER_H
