//
// Created by Christian Horst on 23.07.21.
//

#include "FileHandler.h"
#include <filesystem>
using namespace std;
namespace fs = std::filesystem;
FileHandler::FileHandler() = default;

//check if files exists in the 'order' folder
int FileHandler::checkForFiles(const std::string& pathName) {
    auto dirIter = std::filesystem::directory_iterator(pathName);
    int fileCount = 0;
    for (auto &entry : dirIter) {
        if (entry.is_regular_file() ) {
            ++fileCount;
        }
    }
    if(fileCount < 1) return -1;
    else return 0;
}

//open yaml file, search for the desired ID and save the corresponding data
void FileHandler::retrieveOrderInformation(const string& pathName, int wantedID){
    cout << "Search for Order ID: " << wantedID << endl;
    cout << "Search for Order ID in File: \n" << pathName << endl;
    YAML::Node order = YAML::LoadFile(pathName);
    currentOrder.orderID = wantedID;
    int iterator = 0;
    int inner_iterator = 0;

    for (YAML::const_iterator it = order.begin(); it != order.end(); ++it) {
        if (wantedID == order[iterator]["order"].as<int>()) {
            currentOrder.orderDestX = order[iterator]["cx"].as<float>();
            currentOrder.orderDestY = order[iterator]["cy"].as<float>();

            for (YAML::const_iterator jt = order[iterator]["products"].begin(); jt != order[iterator]["products"].end(); ++jt) {
                currentOrder.productList.push_back(order[iterator]["products"][inner_iterator].as<int>());
                ++inner_iterator;
            }
            orderFound = true;
        }
        ++iterator;

    }
}

set<fs::path> FileHandler::getFiles(string pathName) {
    set<fs::path> allFiles;

    for (auto &entry : fs::directory_iterator(pathName))
        allFiles.insert(entry.path());
    return allFiles;
}

//open the yaml file to retrieve the information about the necessary parts and their pick up locations
void FileHandler::retrieveProductInformation() {
    string completePath = configPathName + "/products.yaml";
    YAML::Node config = YAML::LoadFile(completePath);

    for (auto & prodIt : currentOrder.productList) {
        int iterator = 0;
        for (YAML::const_iterator it = config.begin(); it != config.end(); ++it) {
            if (prodIt == config[iterator]["id"].as<int>()) {

                int inner_iterator = 0;
                for (YAML::const_iterator jt = config[iterator]["parts"].begin(); jt != config[iterator]["parts"].end(); ++jt) {
                    DataHandler::Part newPart(config[iterator]["parts"][inner_iterator]["part"].as<string>(),config[iterator]["parts"][inner_iterator]["cx"].as<float>(),
                                 config[iterator]["parts"][inner_iterator]["cy"].as<float>(),config[iterator]["id"].as<int>());
                    partList.push_back(newPart);
                    ++inner_iterator;
                }
            }
            ++iterator;
        }
    }
}

void FileHandler::clearData() {
    partList.clear();
    currentOrder.productList.clear();
}

