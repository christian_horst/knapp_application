#include <utility>

//
// Created by christian on 23.07.21.
//

#ifndef KNAPP_TASK_FINAL_DATAHANDLER_H
#define KNAPP_TASK_FINAL_DATAHANDLER_H


class DataHandler {
public:
    struct Part
    {
        std::string partName;
        float partX,partY;
        int productNumber;
        int pickupOrder;
        explicit Part(std::string n="Default", float px = 0, float py=0, int pN=0, int pO=0) : partName(std::move(n)),partX(px), partY(py), productNumber(pN), pickupOrder(pO){}

    };
    struct Order
    {
        int orderID;
        float orderDestX, orderDestY;
        std::list<int> productList;
        std::string description;
        explicit Order(int ord=0, float dx=0, float dy=0, std::string s="default") : orderID(ord), orderDestX(dx), orderDestY(dy), description(s){}

    };

};


#endif //KNAPP_TASK_FINAL_DATAHANDLER_H
