//
// Created by Christian Horst on 23.07.21.
//
#include <chrono>
#include <memory>
#include "yaml-cpp/yaml.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"
//#include "interfaces/msg/num.hpp"
#include "interfaces/msg/order.hpp"
#include <filesystem>
#include <thread>
#include "visualization_msgs/msg/marker_array.hpp"
#include "FileHandler.h"
#include "FileHandler.cpp"
#include "DataHandler.h"
#include "PathPlanner.h"
#include "PathPlanner.cpp"
#include <thread>


//path planner shortest sum int to float ###
//KOMMENTARE! ###
//STRING AN NODe ÜBERGEBEN
//FEHLERHANDLING
//Hochladen !
//kleine Readme schreiben

using namespace std::chrono_literals;
using std::placeholders::_1;
using namespace std;

class OrderOptimizer : public rclcpp::Node{
public:
    OrderOptimizer() : Node("OrderOptimizer") {

        position_subscription = this->create_subscription<geometry_msgs::msg::PoseStamped>(
                "currentPosition", 10,
                std::bind(&OrderOptimizer::positionCallback,this, _1));
        order_subscription = this->create_subscription<interfaces::msg::Order>(
                "newOrder", 1,
                std::bind(&OrderOptimizer::orderCallback,this, _1));
        visualization_publisher = this->create_publisher<visualization_msgs::msg::MarkerArray>("order_path", 1);
        this->declare_parameter<std::string>("order_path", "/home/christian/dev_ws/src/knapp_task_final/file_storage/orders");
        this->declare_parameter<std::string>("config_path", "/home/christian/dev_ws/src/knapp_task_final/file_storage/configuration");
    }

private:
    void positionCallback(const geometry_msgs::msg::PoseStamped::SharedPtr position ){
        RCLCPP_INFO(this->get_logger(),"I heard a position %f", position->pose.position.x);
        robotX = position->pose.position.x;
        robotY = position->pose.position.y;
    }
    //This callback function processes the important tasks in order to handle the current order
    void orderCallback(const interfaces::msg::Order::SharedPtr order) {

        RCLCPP_INFO(this->get_logger(), "I heard a new order %u", order->order_id);
        this->get_parameter("order_path", pathToOrderFiles);
        this->get_parameter("config_path", pathToConfigFiles);
        cout << "Path to order files: " << pathToOrderFiles << endl;
        cout << "Path to config files: \n" << pathToConfigFiles << endl;
        //save OrderID
        currentOrderID = order->order_id;
        currentOrderDescription = order->description;
        //check if orderID is valid

        //string pathToYAML = "/home/christian/dev_ws/src/knapp_task_final/file_storage/orders";
        //Set the path for the order files and the config files so the Filehander can open them
        handleFiles.setOrderPath(pathToOrderFiles);
        handleFiles.setConfigPath(pathToConfigFiles);

        //check if files exists
        int filesExist= handleFiles.checkForFiles(pathToOrderFiles);
        if(filesExist == -1){
            RCLCPP_INFO(this->get_logger(), "Wrong path or no files found. Order cannot be processed!");
            return;
        }

        //get the path for each existing file in the folder 'order'
        set<fs::path> orderFilesPathSet = handleFiles.getFiles(pathToOrderFiles);
        //Start a new thread for each yaml file in the 'order' folder and search for the desired order ID to retrieve the necessary data
        std::vector<std::thread> threads;
        handleFiles.setOrderFound(false);
       for (const auto & jt : orderFilesPathSet) {
            if (!handleFiles.checkOrderFound()) {
                threads.emplace_back(&FileHandler::retrieveOrderInformation, &handleFiles, jt, currentOrderID);
            }
        }
        for (auto &thread : threads) {
            //wait for all threads to be finished
            thread.join();
        }
        threads.clear();
        //Check if after searching the yaml files the order still could not be found
        if (!handleFiles.checkOrderFound()){
            RCLCPP_INFO(this->get_logger(), "Could not find the desired Order ID. Make sure the ID is correct.");
            return;
        }

        //Get info about the order, retrieved from the order yaml file
        DataHandler::Order newOrder = handleFiles.getOrderInfo();
        newOrder.description=currentOrderDescription;
        newOrder.orderID = currentOrderID;

        //search config yaml for the information of the products belonging to the current order
        handleFiles.retrieveProductInformation();
        // get all the information about all parts which needs to be picked up for the specific order
        std::list<DataHandler::Part> currentPartList = handleFiles.getProductInfo();
        handleFiles.setOrderFound(false);

        //Sort the Part List in alphabetical order
        std::list<DataHandler::Part> sortedPartList = pathPlanner.sortPartListString(currentPartList);

        //Create a reduced part list with only the single distinctive parts
        // This is important to prepare the data for the path planner, since each part has a pick up location which represents
        // a node
        std::list<DataHandler::Part> currentReducedPartList = pathPlanner.reducePartList(sortedPartList);

        //Initialize the matrix for the path planner
        //The starting node is the current position of the robot, location of "Part A" is node 1, "Part B" is node 2 and so on...
        pathPlanner.initializeGraph(robotX,robotY,currentReducedPartList);

        //Get the lowest cost (distance) to visit all nodes
        optimalPathCost = pathPlanner.calculateShortestPath(currentReducedPartList);

        //order the part list the specific pick up order to travel the shortest distance to each node (part pick-up location)
        std::list<DataHandler::Part> orderedPartList = pathPlanner.rearrangePartOrder(sortedPartList);

        //print the final data
        displayAMR(newOrder,orderedPartList);

        //publish the robot position as a cube and the pickup locations as cylinders
        //it can be visualized with rViz2
        publishMarkers(currentReducedPartList);
        handleFiles.clearData();


    }
    void displayAMR(const DataHandler::Order& currentOrder, const std::list<DataHandler::Part>& finalPartList) const{
        cout << "Working on Order: " << currentOrder.orderID <<  "\t with Description: " << currentOrder.description << endl;
        cout << "Length of optimal Path: " << optimalPathCost << endl;
        for(auto & it : finalPartList){
            cout << "Fetching : " << it.partName << " for Product " << it.productNumber <<  " at x: " <<it.partX <<  " and y: " <<it.partY << endl;
        }
        cout << "Delivering to Destination x: " << currentOrder.orderDestX <<  " and y: " << currentOrder.orderDestY << endl;
    }

    void publishMarkers(const std::list<DataHandler::Part>& reducedPartList){
        visualization_msgs::msg::MarkerArray waypoints;
        visualization_msgs::msg::Marker robotPosition;

        robotPosition.header.frame_id = "map";
        robotPosition.id = 0;
        robotPosition.type =  visualization_msgs::msg::Marker::CUBE;
        robotPosition.action =  visualization_msgs::msg::Marker::ADD;
        robotPosition.scale.x = 10.0;
        robotPosition.scale.y = 10.0;
        robotPosition.scale.z = 10.0;
        robotPosition.color.r = 0.5;
        robotPosition.color.a = 0.7 ;
        robotPosition.pose.orientation.w = 1.0;
        robotPosition.pose.position.x = robotX;
        robotPosition.pose.position.y = robotY;
        robotPosition.pose.position.z = 0.0;
        waypoints.markers.push_back(robotPosition);

        int iterator = 0;
        for(auto & it : reducedPartList){
            visualization_msgs::msg::Marker currentPoint;
            currentPoint.header.frame_id = "map";
            currentPoint.id = 1+iterator;
            currentPoint.type =  visualization_msgs::msg::Marker::CYLINDER;
            currentPoint.action =  visualization_msgs::msg::Marker::ADD;
            currentPoint.scale.x = 10.0;
            currentPoint.scale.y = 10.0;
            currentPoint.scale.z = 10.0;
            currentPoint.color.r = 0.5;
            currentPoint.color.a = 0.7 ;
            currentPoint.pose.orientation.w = 1.0;
            currentPoint.pose.position.x = it.partX;
            currentPoint.pose.position.y = it.partY;
            currentPoint.pose.position.z = 0.0;
            waypoints.markers.push_back(currentPoint);
            ++iterator;
        }
        visualization_publisher->publish(waypoints);
    }

    //ROS specific data
    rclcpp::Subscription<geometry_msgs::msg::PoseStamped>::SharedPtr position_subscription;
    rclcpp::Subscription<interfaces::msg::Order>::SharedPtr order_subscription;
    rclcpp::Publisher<visualization_msgs::msg::MarkerArray>::SharedPtr visualization_publisher;

    //Creating necessary objects
    FileHandler handleFiles;
    PathPlanner pathPlanner;
    //Data for processing the order
    double robotX=0, robotY=0;
    int currentOrderID;
    float optimalPathCost;
    string currentOrderDescription;
    string pathToOrderFiles;
    string pathToConfigFiles;

};

int main(int argc, char *argv[]) {

    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<OrderOptimizer>());
    rclcpp::shutdown();
    return 0;
}