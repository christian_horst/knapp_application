-  Clone this package in your ROS workspace

- This package contains a custom ROS message which is defined in the package 'interfaces' so that package needs to be cloned and build as well

- To parse the yaml files the yaml-cpp library needs to be installed. Install libyaml-cpp-dev by entering the following commands in the terminal: 


```
sudo apt update
sudo apt install libyaml-cpp-dev
```
- Start the implemented ROS Node with: 

`ros2 run knapp_task_final OrderOptimizer`

- Set the paths for the order.yaml and products.yaml via ROS Parameter
```
ros2 param set "order_path" "path where the order.yaml files are stored"
ros2 param set "config_path" "path where the products.yaml files are stored"
```
- After this the Node waits for a new order to be published and starts processing it when it receives one
